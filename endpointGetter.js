//var request = require("request");
var cheerio = require("cheerio");
const puppeteer = require("puppeteer");
const fs = require("fs");

const indeedLink = "https://resumes.indeed.com";
let timeoutDuration = 1000; //NOT USED
let npages = 21; //change this

const getResumeURL = async (pid, query, location) => {
  const browser = await puppeteer.launch({
    args: ["--no-sandbox", "--disable-setuid-sandbox"]
  });
  try {
    const page = await browser.newPage();
    const url = `https://resumes.indeed.com/search?q=${query}&l=${location}&searchFields=jt&start=${pid *
      50}`;
    // DEBUG ONLY
    // console.log(url);

    // Set View
    await page.setUserAgent(
      "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"
    );
    await page.setViewport({ width: 1366, height: 768 });

    // Force change location
    await page.goto("https://resumes.indeed.com/?co=US");
    // DEBUG ONLY
    // await page.screenshot({ path: 'home1.png' });
    await page.goto(url, { waitUntil: "networkidle0" });
    // DEBUG ONLY
    // await page.screenshot({ path: "test1.png" });
    await page.waitForSelector(".rezemp-ResumeSearchCard-contents", {
      timeout: timeoutDuration
    });

    // Load Body and assign to $
    const body = await page.evaluate(() => {
      return document.querySelector("body").innerHTML;
    });
    let $ = cheerio.load(body);

    let data = [];

    $(".rezemp-ResumeSearchCard").each(function(i, element) {
      data[i] = `${pid * 50 + i} ${indeedLink}${$(this)
        .find("div > div > span > a")
        .attr("href")}`;
    });
    console.log(data.join("\n"));
    await browser.close();
    // END
  } catch (error) {
    fs.appendFileSync("r.log", `xpage${pid}/${query}/${location} ${error}\n`);
    // console.log(`xpage${pid} ${error}`);
    await browser.close();
  }
};

// Step 1

(async () => {
  query = [
    "software engineer",
    "data engineer",
    "data scientist",
    "analyst",
    "data analyst",
    "consulting",
    "audit",
    "developer",
    "architect",
    "infrastructure",
    "support",
    "network",
    "backend",
    "frontend",
    "system",
    "administrator",
    "devops",
    "mobile",
    "quality",
    "business",
    "intelligence",
    "database",
    "marketing",
    "designer",
    "operation",
    "product manager",
    "product marketer",
    "product designer",
    "web",
    "python",
    "java",
    "android",
    "ios",
    "junior",
    "senior",
    "php",
    "ruby",
    "consultant",
    "founder",
    "linux",
    "assistant",
    "game developer",
    "test engineer",
    "javascript",
    "react",
    "angular"
  ];
  location = [
    "california",
    "Colorado",
    "Idaho",
    "Illinois",
    "Carolina",
    "Indiana",
    "New York",
    "Massachusetts",
    "Hampshire",
    "Washington",
    "Ithaca",
    "Texas",
    "Jersey",
    "Oregon",
    "Vermont",
    "Michigan",
    "Minnesota",
    "Rochester",
    ""
  ];
  // for (let i = 0; i < npages; i++) {
  let i = 1;
  for (let j = 0; j < query.length; j++) {
    for (let k = 0; k < location.length; k++) {
      await getResumeURL(i, query[j], location[k]);
    }
  }
  //}
})();

// Step 2
// let file = fs.readFileSync("output.txt").toString();
// let listOfUri = file.split("\n");
// listOfUri.forEach((val, i) => {
//   let uri = val.split(" ")[1];
//   console.log(i, ".this uri", uri);
// try {
// const browser = await puppeteer.launch();
//   const page = await browser.newPage();
//   await page.goto(
//     `https://resumes.indeed.com/search?q=software+engineer&l=&searchFields=jt&start=${id *
//       50}`
//   );
//   await page.waitForSelector(".rezemp-ResumeSearchCard-contents", {
//     timeout: 10000
//   });

//   const body = await page.evaluate(() => {
//     return document.querySelector("body").innerHTML;
//   });

//   const $ = cheerio.load(body);
// } catch (ex) {
//   console.log(ex);
// }
// });
