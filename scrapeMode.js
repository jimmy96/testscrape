//var request = require("request");
var cheerio = require("cheerio");
const puppeteer = require("puppeteer");
const fs = require("fs");

// const indeedLink = "https://resumes.indeed.com";
let timeoutDuration = 500;

const scrapeMode = async (id, uri) => {
  const browser = await puppeteer.launch();
  try {
    const page = await browser.newPage();

    // Set View
    await page.setUserAgent(
      "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"
    );
    await page.setViewport({ width: 1366, height: 768 });

    // Force change location
    await page.goto("https://resumes.indeed.com/?co=US");

    await page.goto(uri, { waitUntil: "networkidle0" });
    await page.waitForSelector(".rezemp-ResumeDisplaySection", {
      timeout: timeoutDuration
    });
    // Load Body and assign to $
    const body = await page.evaluate(() => {
      return document.querySelector("body").innerHTML;
    });
    const $ = cheerio.load(body);

    let data = [];
    data.push(`${id} `);
    data.push($(".rezemp-u-h2").html());
    data.push("dob");
    data.push("addr");
    data.push("ph");
    data.push("em");

    // data.push($(".rezemp-ResumeDisplaySection").html);

    // console.log($(".rezemp-ResumeDisplaySection").length);
    let sectionCount = $(".rezemp-ResumeDisplaySection").length;
    $(".rezemp-ResumeDisplaySection").each(function(i, e) {
      if (i < 3) {
        data.push(`"${$(this).html()}"`);
      }
    });
    // console.log(data.join(","));
    console.log(clean(data.join(",")));

    await browser.close();
  } catch (error) {
    console.log(`${id},${error}`);
    await browser.close();
  }
};

function clean(str) {
  let res = str.replace(/<[^>]*>/gim, " ");
  res = res.replace(/\s*([,\)])\s*/gi, "$1 ");
  res = res.replace(/\s*([\"\(])\s*/gi, " $1");
  res = res.replace(/\s+/gim, " ");
  return res;
}

// Step 2
(async () => {
  file = fs.readFileSync("orderedResume.list", { encoding: "utf8" });
  let rawUriList = file.trim().split("\n");
  for (let i = 0; i < rawUriList.length; i++) {
    let uri = rawUriList[i].split(/\s+/);
    let no = uri[0];
    let url = uri[1];
    await scrapeMode(no, url);
  }
})();
