const fs = require("fs");
a = fs.readFileSync("resume.list", { encoding: "utf8" });

arr = a.trim().split("\n");
for (let i = 0; i < arr.length; i++) {
  const element = arr[i].trim().split(/\s+/);
  if (element.length > 1) {
    arr[i] = `${i} ${element[1]}`;
  }
}

for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}
